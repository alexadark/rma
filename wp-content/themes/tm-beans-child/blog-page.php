<?php
//Template Name: Blog

beans_add_smart_action( 'beans_content', 'beans_loop_template' );
add_filter( 'beans_loop_query_args', 'blog_loop_query_args' );
function blog_loop_query_args() {

	return array(
		'posts_per_page' => 10,
		'paged' => get_query_var( 'paged' ),
	);

}
add_action('beans_post_header','beans_post_title');



beans_load_document();