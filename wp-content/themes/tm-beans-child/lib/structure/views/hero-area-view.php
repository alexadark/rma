<?php
$image_id   = esc_attr( carbon_get_the_post_meta( 'crb_hero_image' ) );
$image_url = wp_get_attachment_image_url($image_id,'full');
$title    = esc_attr( carbon_get_the_post_meta( 'crb_hero_title' ) );
$subtitle = esc_attr( carbon_get_the_post_meta( 'crb_hero_subtitle' ) );
$label    = esc_attr( carbon_get_the_post_meta( 'crb_hero_button_label' ) );
$link     = esc_attr( carbon_get_the_post_meta( 'crb_hero_link' ) );
if($image_id){
?>
<section class="hero-area uk-flex uk-flex-center uk-flex-middle uk-flex-column uk-text-center"
         style="
	         background: url(<?php echo $image_url; ?>) no-repeat;
	         -webkit-background-size: cover;
	         background-size: cover;
	         ">
	<div class="hero-content uk-width-medium-2-3 uk-container-center">
		<div class="hero-title"><?php echo $title; ?></div>
	<div class="hero-subtitle"><?php echo $subtitle ?></div>
	<?php if ( $label ): ?>
		<a href="<?php echo $link; ?>"
		   class="hero-link uk-button"><?php echo $label; ?></a>
	<?php endif; ?>
	</div>

</section>
<?php }
