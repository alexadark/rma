<?php
$section_title = esc_html(carbon_get_the_post_meta('crb_slider_title'));
$button_label = esc_html(carbon_get_the_post_meta('crb_slider_button_label'));
$button_link = esc_html(carbon_get_the_post_meta('crb_slider_button_link'));
?>
<div class="section slider-section uk-margin-large-top">
	<h3 class="grey-title uk-text-center"><?php echo $section_title;?></h3>
	<?php wst_get_layout('slider/views/slider-view.php'); ?>


<!--	<a href="--><?php //echo $button_link; ?><!--"-->
<!--	   class="uk-button uk-align-center">--><?php //echo $button_label?><!--</a>-->

</div>
