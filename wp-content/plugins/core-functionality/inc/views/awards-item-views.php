<?php
$title = esc_html($item['crb_award_item']);
$subtitle = esc_html($item['crb_award_item_subtitle']);
?>
<div class="award-item ">
	<h5 ><strong><?php echo $title; ?></strong></h5>
	<div class="muted-text"><?php echo $subtitle; ?></div>
</div>

