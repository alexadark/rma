<?php

$logo           = wp_get_attachment_image( carbon_get_the_post_meta('crb_slide_logo'), 'full' );
$slide_subtitle = esc_html( carbon_get_the_post_meta('crb_slide_subtitle') );
$text           = carbon_get_the_post_meta('crb_slide_text');
$image          = wp_get_attachment_image( carbon_get_the_post_meta('crb_slide_image'), 'full' );
$video          = carbon_get_the_post_meta('crb_video_url');
$link = $video ? $video : get_the_permalink();
?>
<li class="works-head">
	<div class="uk-panel uk-panel-box">
		<div class="uk-grid ">
			<div class="tm-slideshow-img  uk-width-large-2-3 uk-push-1-3">
				<?php if ( $video ): ?>
				<a href="<?php echo $video ?>" >
					<?php endif; ?>
					<?php echo $image; ?>
					<?php if ( $video ): ?>
				</a>
			<?php endif; ?>
			</div>

			<div class="tm-slideshow-text uk-width-large-1-3 uk-pull-2-3">
				<div class="slider-logo">
					<?php echo $logo; ?>
				</div>
				<div class="slide-title"><strong><?php the_title(); ?></strong></div>
				<div class="slide-subtitle"><?php echo $slide_subtitle; ?></div>
				<div class="slide-text"><?php echo $text; ?></div>

			</div>

		</div>
		<?php if (!is_singular('work')):?>
		<a href="<?php echo $link; ?>"
		   class="uk-button uk-align-center">Find out more</a>
		<?php endif;?>

	</div>
</li>
