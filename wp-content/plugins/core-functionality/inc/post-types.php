<?php
// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'wst_flush_rewrite_rules' );

// Flush your rewrite rules
function wst_flush_rewrite_rules() {
	flush_rewrite_rules();
}
add_action( 'init', 'register_careers_post_type' );
function register_careers_post_type() {

	$labels = array(
		'name'          => _x( 'Careers', 'post type general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( 'Career', 'post type singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Careers', 'admin menu name', CHILD_TEXT_DOMAIN ),
		'add_new'       => _x( 'Add New Career', 'faq', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => _x( 'Add New Career', CHILD_TEXT_DOMAIN ),
		'search_items'  => _x( 'Search Career', CHILD_TEXT_DOMAIN ),
		'not_found'     => _x( 'No Career Found', CHILD_TEXT_DOMAIN ),

	);
	$args   = array(
		'label'        => __( 'Careers', CHILD_TEXT_DOMAIN ),
		'labels'       => $labels,
		'supports'     => get_cpt_supports(),
		'public'       => true,
		'taxonomies'   => array(),
		'hierarchical' => true,
		'has_archive'  => false,
	);

	register_post_type( 'careers', $args );
}

add_action( 'init', 'register_work_post_type' );
function register_work_post_type() {

	$labels = array(
		'name'          => _x( 'Works', 'post type general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( 'Work', 'post type singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Works', 'admin menu name', CHILD_TEXT_DOMAIN ),
		'add_new'       => _x( 'Add New Work', 'faq', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => _x( 'Add New Work', CHILD_TEXT_DOMAIN ),
		'search_items'  => _x( 'Search Work', CHILD_TEXT_DOMAIN ),
		'not_found'     => _x( 'No Work Found', CHILD_TEXT_DOMAIN ),

	);
	$args   = array(
		'label'        => __( 'Works', CHILD_TEXT_DOMAIN ),
		'labels'       => $labels,
		'supports'     => get_cpt_supports(),
		'public'       => true,
		'taxonomies'   => array(),
		'hierarchical' => true,
		'has_archive'  => false,
	);

	register_post_type( 'work', $args );
}

function get_cpt_supports() {
	$all_supports = get_all_post_type_supports( 'post' );


	$all_supports = array_keys( $all_supports );

	$supports_to_exclude = array(
		'comments',
		'trackbacks',
		'post_formats',
		'custom-fields',
	);


	$supports   = array_filter( $all_supports, function ( $support ) use ( $supports_to_exclude ) {
		return ! in_array( $support, $supports_to_exclude );
	} );
	$supports[] = 'page-attributes';

	return ( $supports );


}
