<div class=" tm-slideshow uk-container uk-container-center uk-margin-large uk-slidenav-position"
     data-uk-slideshow="{autoplay:true,
		animation:'swipe',height:580}">
	<ul class="uk-slideshow">
<!--		--><?php //wst_get_simple_slides('crb_slides','views/slides-view.php');?>
		<?php display_works();?>


	</ul>
	<a href=""
	   class="uk-slidenav uk-visible-large  uk-slidenav-previous"
	   data-uk-slideshow-item="previous"></a>
	<a href=""
	   class="uk-slidenav uk-visible-large  uk-slidenav-next"
	   data-uk-slideshow-item="next"></a>
	<ul class="uk-dotnav uk-hidden-small uk-position-bottom uk-flex-center">
		<?php wst_get_simple_dotnav_items('crb_slides');?>
	</ul>
</div>