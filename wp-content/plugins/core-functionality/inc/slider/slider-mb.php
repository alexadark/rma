<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', 'Works custom fields' )
         ->show_on_post_type( 'work' )
         ->add_fields( array(
		              Field::make( 'image', 'crb_slide_logo' ),
		              Field::make( 'text', 'crb_slide_subtitle' ),
		              Field::make( 'rich_text', 'crb_slide_text' ),
		              Field::make( 'image', 'crb_slide_image' ),
		              Field::make( 'text', 'crb_video_url' ),
         ) );


