<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta','People')
	->show_on_post_type( 'page' )
	->show_on_template( 'about_page.php' )
	->add_fields(array(
		Field::make('complex','crb_people_pics')->set_layout('tabbed')
		->add_fields(array(
			Field::make('image','crb_people_image')
		)),
		Field::make('complex','crb_people_switcher_content')->set_layout('tabbed')
		->add_fields(array(
			Field::make('text','crb_people_name'),
			Field::make('text','crb_people_role'),
			Field::make('textarea','crb_people_content'),
		))


	));