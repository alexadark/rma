<?php

beans_add_smart_action( 'beans_post_header_prepend_markup', 'display_team' );
function display_team() {
	$team = esc_attr(carbon_get_the_post_meta('crb_team'));

?>
	<div class="uk-text-muted"><?php echo $team;?></div>
<?php }

beans_add_smart_action( 'beans_post_header_append_markup', 'display_timing_and_location' );
function display_timing_and_location() {

	$timing = esc_attr(carbon_get_the_post_meta('crb_availability'));
	$location = esc_attr(carbon_get_the_post_meta('crb_location'));
	?>
	<div class="timing"><?php echo $timing;?></div>
	<div class="location uk-text-muted"><?php echo $location; ?></div>
	<hr>
<?php }

beans_add_smart_action( 'beans_post_content_prepend_markup', 'display_career_excerpt' );
function display_career_excerpt() {
	echo '<h5 class="career-excerpt">';
	the_excerpt();
	echo '</h5>';
	}

beans_add_smart_action( 'beans_post_content_append_markup', 'display_apply_button' );
function display_apply_button() { ?>
	<a href="mailto:jobs@rma-consulting.com"
	   class="uk-button">Apply for this job</a>
	<hr>
<?php }

beans_add_smart_action( 'beans_post_content_after_markup', 'wst_display_about_rma' );
function wst_display_about_rma() {
echo beans_widget_area('about_rma');
	}

beans_load_document();