<?php

beans_add_smart_action( 'wp', 'wst_set_up_post_structure' );
function wst_set_up_post_structure() {
	//Remove title only on pages
	if ( is_singular( 'page' ) ) {

		beans_remove_action( 'beans_post_title' );
	}

	// Force layout.
	if ( is_singular( 'work' ) ) {
		add_filter( 'beans_layout', 'example_force_layout' );
	}
	function example_force_layout() {
		return 'c';
	}

	add_filter( 'the_content', 'wst_modify_post_content' );


	function wst_modify_post_content( $content ) {

		// Stop here if we are on a single view.
		if ( is_singular() ) {
			return $content;
		}

		// Return the excerpt() if it exists other truncate.
		if ( has_excerpt() ) {
			$content = '<p>' . get_the_excerpt() . '</p>';
		} else {
			$content = '<p>' . wp_trim_words( get_the_content(), 50, '...' ) . '</p>';
		}

		// Return content and readmore.
		return $content;

	}

	beans_remove_action( 'beans_post_navigation' );

	beans_remove_action( 'beans_post_meta_date_shortcode' );
	add_action( 'beans_post_meta_append_markup', 'beans_post_meta_date_shortcode' );
	beans_remove_output( 'beans_post_meta_date_prefix' );
	beans_remove_action( 'beans_post_meta_author_shortcode' );
	add_action( 'beans_post_meta_prepend_markup', 'new_author_output' );
	function new_author_output() { ?>
		<a href="<?php the_author_link(); ?>">
			by <?php the_author(); ?>
		</a>
	<?php }

	beans_remove_action( 'beans_post_meta_tags' );

	beans_modify_action_hook( 'beans_post_meta_categories', 'beans_post_header_append_markup' );
	beans_remove_output( 'beans_post_meta_categories_prefix' );
	if ( is_page_template( 'blog-page.php' ) ) {
		beans_add_smart_action( 'beans_post_append_markup', 'display_hr' );
		function display_hr() {
			echo '<hr>';
		}
	}


}
