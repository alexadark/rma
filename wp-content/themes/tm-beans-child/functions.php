<?php

include_once( 'lib/init.php' );

// Include Beans. Do not remove the line below.
require_once( get_template_directory() . '/lib/init.php' );

include_once( 'lib/functions/autoload.php' );

function display_works() {
	$args = array(
		'post_type' => 'work',

	);
	if(is_front_page()){
		$args = array(
			'post_type' => 'work',
			'tax_query' => array(
				array(
					'taxonomy' => 'works-categories',
					'field' => 'slug',
					'terms' => 'slider',
				),
			),

		);
	}

	$the_query = new WP_Query( $args );

// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			include ('views/works-header-view.php');
		}
	}


// Reset original post data
	wp_reset_postdata();

 }