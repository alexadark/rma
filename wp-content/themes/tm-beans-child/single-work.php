<?php

beans_remove_action('beans_post_title');

beans_add_smart_action( 'beans_post_header_prepend_markup', 'wst_get_works_header' );
function wst_get_works_header() {
	include ('views/works-header-view.php');
}

beans_add_smart_action( 'beans_footer_before_markup', 'wst_display_quote' );
function wst_display_quote() {
	include ('views/blockquote-view.php');
	}

beans_add_smart_action( 'beans_footer_before_markup', 'wst_display_awards' );
function wst_display_awards() {
	include ('views/awards-view.php');
		}

beans_load_document();