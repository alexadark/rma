<div class="uk-grid">
    <div class="uk-width-1-4">
	    <img src="<?php echo CHILD_IMG ?>logo-footer.png"
	         alt="RMA"
	         class="footer-logo">
    </div>
    <div class="uk-width-3-4">
	    <div class="uk-grid">
		    <div class="footer-nav uk-width-9-10">
			    <?php echo beans_widget_area( 'footer_menu' ); ?>
		    </div>
		    <div class="footer-icon uk-width-1-10">
		    <a href="#"
		       class="uk-icon-medium uk-icon-twitter"></a>
		    </div>
	    </div>
	    <div class="uk-grid">
	        <div class="uk-width-9-10 address">
		        RMA Consulting, 13-21 Curtain Road, Shoreditch, London EC2A3LT
		        <a href="mailto:hello@rma-consulting.com"> <i class="uk-icon-envelope uk-icon-button"></i> hello@rma-consulting.com </a><i class="uk-icon-phone uk-icon-button"></i> 020 7123 4567
	        </div>
	        <div class="uk-width-1-10">
		        <a href="#"
		           class="uk-icon-medium uk-icon-facebook-official"></a>
	        </div>
	    </div>
	    <div class="uk-grid">
	        <div class="uk-width-9-10 copyright uk-flex uk-flex-right">
		        © <?php echo date('Y');?> RMA Consulting. All rights reserved | Legal Information | Company Nr: 05656677
	        </div>
	        <div class="uk-width-1-10">
		        <a href="#"
		           class="uk-icon-medium uk-icon-linkedin"></a>
	        </div>
	    </div>


    </div>
</div>
