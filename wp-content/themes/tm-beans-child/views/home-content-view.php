<?php

$image = wp_get_attachment_image_url(carbon_get_the_post_meta('crb_left_image'),'full');
$logos = wp_get_attachment_image(carbon_get_the_post_meta('crb_logos'),'full');
?>
<section class="section home-content ">
	<div class="uk-grid uk-grid-width-large-1-2 uk-grid-collapse">
		<div class="left-image"
		style ="background:url(<?php echo $image; ?>) no-repeat;
			-webkit-background-size:cover ;background-size: cover;">

		</div>
		<div class="grey-area">
			<div class="content-wrap">
			<?php the_content(); ?>
			</div>
		</div>
	</div>
	<div class="logos uk-container uk-container-center uk-margin-large">
		<?php echo $logos?>
	</div>



</section>