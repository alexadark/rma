<?php
/**
 * Display a slider with complex field slides
 *
 * @since 1.0.0
 *
 * @param $slider_view_path : path to the slider view from thos function file
 *
 * @return void
 */
function wst_get_simple_slider($slider_view_path){
	include($slider_view_path);

}

/**
 * Get the slides for the slider
 *
 * @since 1.0.0
 *
 * @param $slides_field : name of the slides complex field
 * @param $slides_view_path :path to the slides view from this function file
 *
 * @return void
 */
function wst_get_simple_slides($slides_field, $slides_view_path){
	$slides = carbon_get_the_post_meta($slides_field,'complex');
	if(!$slides){
		return;
	}
	foreach ( $slides as $slide ) {
		include($slides_view_path);
		 }

}

/**
 * Display the dotnav in sliders
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_get_simple_dotnav_items( $slides_field ) {
	$slides = carbon_get_the_post_meta($slides_field,'complex');
	if(!$slides){
		return;
	}
	$total_nb_of_slides = count( $slides );
	for ( $nb_of_slides = 0; $nb_of_slides < $total_nb_of_slides; $nb_of_slides ++ ) : ?>
		<li data-uk-slideshow-item="<?php echo (int) $nb_of_slides; ?>"><a href="#"></a></li>
	<?php endfor;
}