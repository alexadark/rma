<?php
$name = esc_html($item['crb_people_name']);
$role = esc_html($item['crb_people_role']);
$content = esc_html($item['crb_people_content']);
?>
<li class="people-content grey-area uk-text-center">
	<h4 class="black"><?php echo $name;?></h4>
	<h5 class="white"><?php echo $role ?></h5>
	<p class="uk-width-medium-1-2 uk-container-center"><?php echo $content?>
	</p>
</li>
