<?php
$logo = wp_get_attachment_image(carbon_get_the_post_meta('crb_logo_award'),'full');
$title = esc_html(carbon_get_the_post_meta('crb_award_title'));
$subtitle = esc_html(carbon_get_the_post_meta('crb_award_subtitle'));
$awards = carbon_get_the_post_meta('crb_awards','complex');
if(empty($awards)){
	return;
}
?>
<section class="section grey-area uk-block awards-section">
	<div class="awards-header uk-text-center">
		<?php echo $logo;?>
		<h3 class="grey-title"><?php echo $title;?></h3>
		<p class="awards-subtitle"><?php echo $subtitle; ?></p>
		<div class="uk-grid uk-grid-width-medium-1-3 uk-container uk-container-center uk-margin-large">
			<?php wst_get_items('crb_awards','views/awards-item-views.php');?>
		</div>


	</div>
</section>

