<?php
add_action( 'widgets_init', 'wst_register_widget_areas' );

function wst_register_widget_areas() {
	beans_register_widget_area( array(
		'name' => 'Careers Sidebar',
		'id' => 'careers_sidebar'
	) );
	beans_register_widget_area( array(
		'name' => 'About RMA',
		'id' => 'about_rma'
	) );
	beans_register_widget_area( array(
		'name' => 'Footer Menu',
		'id' => 'footer_menu'
	) );
	beans_register_widget_area( array(
		'name' => 'About Side',
		'id' => 'about_side',
//		'beans_type' => 'grid'
	) );
	beans_register_widget_area( array(
		'name' => 'About Orange Section',
		'id' => 'about_orange',
	) );



}


