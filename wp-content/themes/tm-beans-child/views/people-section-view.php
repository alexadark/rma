<div class="section people-section ">
	<h3 class="grey-title uk-text-center">Meet (some of) our problem solvers</h3>
	<div class="people-switcher">
		<ul class="switcher-nav uk-grid uk-grid-width-1-4 uk-grid-width-medium-1-8" data-uk-switcher="{connect:'#people'}">
			<li class="dn"><a href="#"></a></li>
			<?php wst_get_items('crb_people_pics','people-switcher/views/switcher-nav-view.php');?>
		</ul>

		<!-- This is the container of the content items -->
		<ul id="people" class="uk-switcher">
			<li class="people-content grey-area uk-text-center">
				<h4 class="black">You want credibility?</h4>
				<h5 class="white">Check out our team principals</h5>
				<p class="uk-width-medium-1-2 uk-container-center">The RMA team is ridiculously well qualified to deliver
					everything you could possible want in the wonderful world of digital solutions. Total awesomeness in
					the business and client serices domains, coolness and mega talent in user experience design
					consultancy, and technical delivery chops like you wouldn’t believe.
				</p>
			</li>
			<?php wst_get_items('crb_people_switcher_content','people-switcher/views/switcher-content-view.php');?>
		</ul>

	</div>


</div>