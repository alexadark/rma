<?php
beans_add_smart_action('wp','wst_set_up_footer_structure');
function wst_set_up_footer_structure(){
	// 10.1 Overwrite the Footer content
	beans_remove_markup('beans_fixed_wrap[_footer]');
	beans_remove_attribute('beans_footer','class','uk-block');
	beans_modify_action_callback( 'beans_footer_content', 'beans_child_footer_content' );

	function beans_child_footer_content() {
		include ('views/footer-view.php');
	}
//footer menu
	beans_replace_attribute('beans_widget_panel[_footer_menu][_nav_menu][_nav_menu-2]','class','uk-panel-box','uk-navbar uk-flex uk-flex-right');

beans_replace_attribute('beans_menu[_sidenav]','class','uk-nav-side', 'uk-navbar-nav ');

}