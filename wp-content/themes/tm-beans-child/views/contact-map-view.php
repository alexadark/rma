<h3 class="h3-bold">Find us</h3>
<div class="uk-grid">
    <div class="gmap uk-width-large-5-6">
	    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2473.8801144247814!2d-0.08302367682943645!3d51.52296090054773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761cb1cc096191%3A0xe4cb680910d53432!2s21+Curtain+Rd%2C+London+EC2A+3LT%2C+Royaume-Uni!5e0!3m2!1sfr!2ses!4v1473406387837" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="contact-info uk-width-large-1-6">
	    <div class="address">
		    <strong>RMA Consulting</strong><br>
		    13-21 Curtain Road,<br>
		    London  EC21 3LTbr <br>
		    Phone 020 xxx xxx <br>

	    </div>
	    <div class="social ">
		    <h3 class="h3-bold">Follow us</h3>
		    <a href="#"  class="uk-align-left" target="_blank">
			    <img src="<?php echo CHILD_IMG ?>fb.png"
		                      alt="facebook" >
		    </a>
		    <a href="#"  class="uk-align-left" target="_blank">
			    <img src="<?php echo CHILD_IMG ?>twitter.png"
			         alt="twitter" >
		    </a>
		    <a href="#"  class="uk-align-left" target="_blank">
			    <img src="<?php echo CHILD_IMG ?>linkedin.png"
			         alt="linkedin" >
		    </a>
	    </div>
    </div>
</div>
