<?php
beans_add_smart_action('wp','wst_set_up_header_structure');
function wst_set_up_header_structure() {
	beans_remove_markup('beans_fixed_wrap[_header]');

	beans_remove_action( 'beans_site_title_tag' );

	//sticky header
	beans_add_attribute( 'beans_header', 'data-uk-sticky', "{top:-300, animation:'uk-animation-slide-top'}" );

	// Breadcrumb
	beans_remove_action( 'beans_breadcrumb' );
	beans_add_smart_action('beans_header_after_markup','rma_hero_area');
	function rma_hero_area(){
		include ('views/hero-area-view.php');
	}

}