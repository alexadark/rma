<?php
$quote  = esc_html( carbon_get_the_post_meta( 'crb_quote' ) );
$author = esc_html( carbon_get_the_post_meta( 'crb_quote_author' ) );
$role   = esc_html( carbon_get_the_post_meta( 'crb_quote_author_role' ) );
if(! $quote){
	return;
}
?>
<section class="orange-section ">
	<div class="uk-container uk-container-center uk-width-medium-3-5">
		<div class="quote"><?php echo $quote; ?></div>
		<div class="quote-author"><?php echo $author; ?></div>
		<div class="quote-role"><?php echo $role; ?></div>
	</div>
</section>