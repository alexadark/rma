<?php
//template Name: careers page
beans_add_smart_action( 'beans_content_append_markup', 'display_permanent_opportunities' );
function display_permanent_opportunities() {
	$args = array(
		'post_type' => 'careers',
		'tax_query' => array(
			array(
				'taxonomy' => 'careers-categories',
				'field'    => 'slug',
				'terms'    => 'permanent-opportunities',
			),
		),
	);

	$the_query = new WP_Query( $args );

// The Loop
	if ( $the_query->have_posts() ) {
		echo '<h3 class="career-category">Permanent Opportunities</h3><hr>';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			include ('views/career-archive-view.php');
		}
	}


// Reset original post data
	wp_reset_postdata();

	?>

<?php }

beans_add_smart_action( 'beans_content_append_markup', 'display_contract_opportunities' );
function display_contract_opportunities() {
	$args = array(
		'post_type' => 'careers',
		'tax_query' => array(
			array(
				'taxonomy' => 'careers-categories',
				'field'    => 'slug',
				'terms'    => 'contract-opportunities',
			),
		),
	);

	$the_query = new WP_Query( $args );

// The Loop
	if ( $the_query->have_posts() ) {
		echo '<h3 class="career-category">Contract Opportunities</h3><hr>';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			include ('views/career-archive-view.php');
		}
	}


// Reset original post data
	wp_reset_postdata();

	?>

<?php }


beans_load_document();