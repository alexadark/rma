
// Slideshow panel
(function ($) {
    $(document).ready(function () {
        $( '.switcher-nav a' ).on( 'hover', function() {
            $( this ).trigger( 'click' );
        } );

        $('.slideshow-panel-animate').on('beforeshow.uk.slideshow', function (e, next) {
            $(this)
                .find('[data-uk-slideshow]')
                .not(next.closest('[data-uk-slideshow]')[0])
                .data('slideshow')
                .show(next.index());
        });

        var $colHeight = $('.grey-area').outerHeight();
        $('.left-image').height($colHeight);

        var $imgWidth = $('.people').width();
        $('.people').height($imgWidth);

        //switcher hover
        //add class to li.people when .switcher-nav is hovered
        $('.switcher-nav').hover(function(){
            $('.people').toggleClass("desaturate");
        })
    });


})(jQuery);
