<?php
add_action('init', 'register_careers_category_taxonomy');

function register_careers_category_taxonomy(){

	$labels = array(
		'name'          => _x( ' Careers Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Career Category', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Careers Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Careers Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Career Category', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Career Category', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Career Category', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Career Category', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('careers-categories','careers', $args);
}

add_action('init', 'register_works_category_taxonomy');

function register_works_category_taxonomy(){

	$labels = array(
		'name'          => _x( ' Works Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Work Category', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Works Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Works Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Work Category', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Work Category', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Work Category', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Work Category', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('works-categories','work', $args);
}