<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', 'Hero Area' )
         ->show_on_post_type( 'page' )
         ->add_fields( array(
	         Field::make( 'image', 'crb_hero_image' ),
	         Field::make( 'textarea', 'crb_hero_title' )->set_rows( 3 ),
	         Field::make( 'textarea', 'crb_hero_subtitle' )->set_rows( 3 ),
	         Field::make( 'text', 'crb_hero_button_label' ),
	         Field::make( 'text', 'crb_hero_link' ),
         ) );
Container::make( 'post_meta', 'Blockquote' )
         ->show_on_post_type( array( 'page', 'work' ) )
//         ->show_on_page( (int) get_option( 'page_on_front' ) )
         ->add_fields( array(
		Field::make( 'textarea', 'crb_quote' )->set_rows( 3 ),
		Field::make( 'text', 'crb_quote_author' ),
		Field::make( 'text', 'crb_quote_author_role' ),
	) );

Container::make( 'post_meta', 'Careers Custom Fields' )
         ->show_on_post_type( array( 'careers' ) )
         ->add_fields( array(
	         Field::make( 'text', 'crb_team' ),
	         Field::make( 'text', 'crb_availability' ),
	         Field::make( 'text', 'crb_location' ),

         ) );
Container::make( 'post_meta', 'Home last block' )
         ->show_on_post_type( array( 'page' ) )
         ->show_on_page( (int) get_option( 'page_on_front' ) )
         ->add_fields( array(
	         Field::make( 'image', 'crb_left_image' ),
	         Field::make( 'image', 'crb_logos' ),
         ) );

Container::make( 'post_meta', 'Awards' )
         ->show_on_post_type( array( 'work' ) )
         ->add_fields( array(
	         Field::make( 'image', 'crb_logo_award' ),
	         Field::make( 'text', 'crb_award_title' ),
	         Field::make( 'text', 'crb_award_subtitle' ),
	         Field::make( 'complex', 'crb_awards' )->set_layout( 'tabbed' )
	              ->add_fields( array(
		              Field::make( 'text', 'crb_award_item' ),
		              Field::make( 'text', 'crb_award_item_subtitle' ),
	              ) )
         ) );
