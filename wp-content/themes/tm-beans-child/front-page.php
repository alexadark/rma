<?php
beans_remove_attribute('beans_fixed_wrap[_main]','class','uk-container uk-container-center');
beans_remove_attribute('beans_main','class','uk-block');
beans_remove_attribute('beans_post','class','uk-panel-box');
beans_remove_action('beans_post_content');
beans_add_smart_action( 'beans_post_body_prepend_markup', 'wst_display_home_areas',1 );
function wst_display_home_areas() {

	include ('views/slider-section-view.php');
	include ('views/blockquote-view.php');
	include('views/home-content-view.php');
	?>

<?php
	}


beans_load_document();