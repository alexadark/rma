<?php
beans_add_smart_action('wp','wst_set_up_sidebars_structure');
function wst_set_up_sidebars_structure(){
	beans_add_attribute( 'beans_widget_panel', 'class', 'uk-panel-box' );
	if(is_singular('careers')||is_page('careers')){
		beans_modify_action_callback('beans_widget_area_sidebar_primary','display_career_sidebar');
		function display_career_sidebar (){
			if(is_singular('careers')) {
				display_apply_button();
			}
			echo beans_widget_area('careers_sidebar');
		}



	}
	beans_replace_attribute('beans_primary','class','uk-width-medium-3-4','uk-width-medium-2-3 uk-width-large-3-4');
	beans_replace_attribute('beans_sidebar_primary','class','uk-width-medium-1-4','uk-width-medium-1-3 uk-width-large-1-4');

	beans_add_attribute('beans_widget_content[_sidebar_primary][_annual_archive_widget][_annual_archive_widget-2]', 'class','annual-archive');
}
