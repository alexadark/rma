<?php
$itle           = esc_html( carbon_get_the_post_meta( 'crb_slider_title' ) );
$logo           = wp_get_attachment_image( $slide['crb_slide_logo'], 'full' );
$slide_title    = esc_html( $slide['crb_slide_title'] );
$slide_subtitle = esc_html( $slide['crb_slide_subtitle'] );
$text           = $slide['crb_slide_text'];
$image          = wp_get_attachment_image( $slide['crb_slide_image'], 'full' );
$video          = $slide['crb_video_url'];
?>
<li>
	<div class="uk-panel uk-panel-box">
		<div class="uk-grid ">

			<div class="tm-slideshow-text uk-width-large-1-3">
				<div class="slider-logo">
					<?php echo $logo; ?>
				</div>
				<div class="slide-title"><?php echo $slide_title; ?></div>
				<div class="slide-subtitle"><?php echo $slide_subtitle; ?></div>
				<div class="slide-text"><?php echo $text; ?></div>

			</div>
			<div class="tm-slideshow-img   uk-align-center uk-width-large-2-3">
				<?php if ( $video ): ?>
				<a href="<?php echo $video ?>"
				   data-uk-lightbox>
					<?php endif; ?>
					<?php echo $image; ?>
					<?php if ( $video ): ?>
				</a>
			<?php endif; ?>
			</div>
		</div>

	</div>
</li>

