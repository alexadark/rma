<?php
//template Name: about page
beans_add_smart_action( 'beans_main_prepend_markup', 'about_sidebar', 15 );
beans_remove_attribute('beans_fixed_wrap[_main]','class','uk-container uk-container-center');
beans_remove_attribute('beans_post','class','uk-panel-box');
beans_remove_attribute('beans_widget_panel_about_orange','class','uk-panel-box');
beans_replace_attribute('beans_main','class','uk-block','uk-margin-large-top');
function about_sidebar() { ?>
<div class="about-sidebar">
	<?php echo beans_widget_area('about_side');?>
</div>
<?php }

beans_add_smart_action( 'beans_post_content_append_markup', 'display_people_section' );
function display_people_section() {
	include ('views/people-section-view.php');
	}

beans_add_smart_action( 'beans_post_content_append_markup', 'display_orange_section' );
function display_orange_section() {
	include ('views/about-orange-section.php');
	}

beans_load_document();