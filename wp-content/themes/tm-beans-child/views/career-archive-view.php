<?php
$team = esc_attr(carbon_get_the_post_meta('crb_team'));
$timing = esc_attr(carbon_get_the_post_meta('crb_availability'));
$location = esc_attr(carbon_get_the_post_meta('crb_location')); ?>
<div class="career uk-panel uk-margin">
	<div class="team uk-text-muted"><?php echo $team; ?></div>
	<h2 class="career-title uk-article-title"><a href="<?php the_permalink();?>"></a><?php the_title(); ?></h2>	<div class="timing"><?php echo $timing;?></div>
	<div class="location uk-text-muted"><?php echo $location; ?></div>
	<div class="uk-text-muted"><?php the_excerpt();?></div>
	<a class="readmore" href="<?php the_permalink();?>">View job details</a>

</div>
